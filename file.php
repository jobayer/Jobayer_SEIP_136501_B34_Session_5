<html>
<body>
<form method="post" action="<?php echo $_SERVER['PHP_SELF'];?>" enctype="multipart/form-data">
    Name: <input type="file" name="fileToUpload">
    <input type="submit" value="Upload Image">
</form>

<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // collect value of input field
    $file = $_FILES['fileToUpload'];
    if (empty($file)) {
        echo "empty!";
    } else {
        echo "File Name: ".$_FILES["fileToUpload"]["name"].'<br />';  //Get file Name
        echo "File Size: ".$_FILES["fileToUpload"]["size"].'<br />';  //Get File size

        $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
        if($check !== false) {
            echo "File is an image - " . $check["mime"] . "<br/>";
        }else{
            echo 'It is not an image file!';
        }
        // if we want to see all information of a file, simlely we use ver_dump() function
        echo '<pre>';
        var_dump($_FILES);
        echo '</pre>';
    }
}
?>
</body>
</html>