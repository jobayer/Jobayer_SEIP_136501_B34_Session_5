<?php
echo $_SERVER['PHP_SELF'];  // OUTPUT: /Jobayer_136501_B34_session_3/server.php
echo "<br>";
echo $_SERVER['SERVER_NAME'];  // OUTPUT:  localhost
echo "<br>";
echo $_SERVER['HTTP_HOST'];  // OUTPUT:  localhost
echo "<br>";
echo $_SERVER['HTTP_REFERER'];  // OUTPUT:
echo "<br>";
echo $_SERVER['HTTP_USER_AGENT'];  // OUTPUT: Mozilla/5.0 (Windows NT 10.0; WOW64; rv:47.0) Gecko/20100101 Firefox/47.0
echo "<br>";
echo $_SERVER['SCRIPT_NAME'];  // OUTPUT:  /Jobayer_136501_B34_session_3/server.php
echo "<br>";
echo $_SERVER['REQUEST_METHOD'];  // OUTPUT:  GET
echo "<br>";
echo $_SERVER['SERVER_SOFTWARE'];  // OUTPUT:  Apache/2.4.18 (Win32) OpenSSL/1.0.2e PHP/7.0.3
echo "<br>";
echo $_SERVER['SERVER_ADMIN'];  // OUTPUT:  postmaster@localhost
echo "<br>";
echo $_SERVER['SCRIPT_URI'];  // OUTPUT:
echo "<br>";
echo $_SERVER['SERVER_SIGNATURE'];  // OUTPUT: Apache/2.4.18 (Win32) OpenSSL/1.0.2e PHP/7.0.3 Server at localhost Port 80
echo "<br>";

echo "<pre>";
var_dump($_SERVER);
echo "</pre>";
