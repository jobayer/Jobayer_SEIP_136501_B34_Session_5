<!DOCTYPE html>
<html lang="en">
<head>
    <title>GET Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>GET Method!</h2>
    <form action="<?php $_SERVER['PHP_SELF']; ?>" method="get">
        <div class="form-group">
            <label for="email">Email: </label>
            <input type="email" class="form-control" name="email" placeholder="Enter email Address...">
        </div>
        <div class="form-group">
            <label for="roll">Password: </label>
            <input type="password" class="form-control" name="pass" placeholder="Enter a Password">
        </div>
        <button type="submit" class="btn btn-default" name="submit">Submit</button>
    </form>
</div>
<br />
<div class="container">
    <?php
    if(isset($_GET['submit'])){
        echo "<h2>". "User can see the infromation from the URL (Address bar)"."</h2><hr>";
        echo 'Email: '.$_GET['email'].'<hr>';
        echo 'Roll No. : '.$_GET['pass'].'<br>';
    }
    ?>
</div>

</body>
</html>