<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>POST Method!</h2>
    <form action="<?php $_SERVER['PHP_SELF']; ?>" method="post">
        <div class="form-group">
            <label for="email">Email:</label>
            <input type="email" class="form-control" name="email" placeholder="Enter email address...">
        </div>
        <div class="form-group">
            <label for="roll">Roll No:</label>
            <input type="text" class="form-control" name="roll" placeholder="Enter Roll no.">
        </div>
        <button type="submit" class="btn btn-default" name="submit">Submit</button>
    </form>
</div>

<div class="container">
    <?php
    if(isset($_POST['submit'])){
        echo "<h2>". "User don't have to see it but we echoed it to understand"."</h2><hr>";
        echo 'Email: '.$_POST['email'].'<hr>';
        echo 'Roll No. : '.$_POST['roll'].'<br>';
    }
    ?>
</div>


</body>
</html>